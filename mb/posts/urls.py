# path is a function
# takes at 3 arguments
# route reg exp, view to launch, reverse url lookup name, kwargs
from django.urls import path

from .views import HomePageView

urlpatterns = [
    path('', HomePageView.as_view(), name='home')
]
