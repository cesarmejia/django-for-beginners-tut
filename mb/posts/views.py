# Import Listview from django.views.generic
# Which generics should I know about?
from django.views.generic import ListView

# Import Post for models.py relative to the cwd
from .models import Post

# ListView returns all the records of given model
# template_name defines the template to use for styling of data
# context_object_name relabels the context object => (dict containing data sent by view)


# HomePageView imported into urls.ply ... URLConfs -> View
class HomePageView(ListView):
    model = Post
    template_name = 'home.html'
    context_object_name = 'all_posts_list'
