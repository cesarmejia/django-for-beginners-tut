# Django for Beginners Tutorial

## Table of Contents

- [x] Introduction
- [x] Chapter 01: Initial Set Up
- [x] Chapter 02: Hello World App
- [x] Chapter 03: Pages App
- [x] Chapter 04: Message Board App
- [x] Chapter 05: Blog App
- [x] Chapter 06: Forms
- [x] Chapter 07: User Accounts
- [x] Chapter 08: Custom User Model
- [x] Chapter 09: User Authentication
- [x] Chapter 10: Bootstrap & HomePageView/SignUpView; Plus Test Suite
- [x] Chapter 11: Password Change and Reset
- [x] Chapter 12: Email - SendGrid integration
- [x] Chapter 13: Newspaper App (not just auth and password flow)
- [x] Chapter 14: Permissions and Authorization
- [x] Chapter 15: Comments
- [x] Conclusion & Additional Resources

## Introduction

- We will build 5 progressively more complex Django web apps

  1. Hello, World
  2. Pages
  3. Message Board
  4. Blog
  5. Newspaper

  - w/ custom user model, email integration, foreign keys, authorization, permissions, and more

- Code uses `Django 2.2` and `Python 3.7`

- Author uses `pipenv` for virtual environment manager

  - I will use `mkvirtualenvwrapper`

- Most websites need the same basic functionality:

  - ability to connect to a database
  - set URL routes
  - display content
  - handle security

- Django a "batteries-included" web framework, includes:

  - user authentication
  - templates, routes, and views
  - admin interface
  - robust security
  - support for multiple database backends

- Django's generic class-based views are powerful db managers

- Chapter 15, custom user models

## Chapter 01: Initial Set Up

- Sebastian McKenzie, the creator of `yarn`

```bash
git config --global user.name "Cesar Napoleon Mejia Leiva"
git config --global user.email "cesar@prompt.com"
```

- `Experienced developers` use `vim`
  - sigh ... it's on my do to-do list

## Chapter 02: Hello World App

- `django-admin startproject project_name .` - create a django project; without project dir nesting

- `settings.py` controls project's settings

- `urls.py` links url request to appropriate view response

- `wsgi.py` Web Server Gateway Interface

- `manage.py` manages project execution scripts

- `python manage.py runserver` - run django web server

- `python manage.py migrate`

  - executes pending migrations

- `python manage.py startapp app_name` - create a django app

- `admin.py` config file for Django Admin app

- `apps.py` config file for created app

- `models.py` where db models are defined

- `tests.py` app-specific tests

- `views.py` response/request logic

- Local apps need to be appended to `INSTALLED_APPS` for the project to run the app

- ??? What is `Signals framework`

- Three files power a web page:

  1. `urls.py` what request are we listening for and points to response logic
  2. `views.py` contains request/response logic
  3. `models.py` connects response logic to db

### Django request/response cycle

`URL` -> `View` -> `Model (typically)` -> `Template`

```quote
Django views determine what content is displayed on a given page
while URLConfs determine where that content is going.
The model contains the content from the database and
the template provides styling for it
```

```quote
Why several urls.py?
Think of the top-level `helloworld_project/urls.py` as the gateway
to various url patterns distinct to each app.
```

## Chapter 03: Pages App

- Design Spec:

  - homepage
  - about page
  - utilize Django's class-based views
  - Django templates

- `Templates` individual HTML files that can be linked together and also include basic logic

- `Function-based generic views` introduced to handle common patterns (e.g. get all objects in a model, get_details of 1 object)

- `Class-based generic views` allow for extension and customization of generic views

- Templates compose with `{% extends 'base.html'}` and `{% block content_name %}` ... `{% endblock content_name %}`

- Jacob Kaplan-Moss, co-creator of Django: `Code without tests is broken as designed.`

- Writing tests is important because it automates the process of confirming that the code works as expected.

- `SimpleTestCase` is used if no db access needed by test, otherwise use `TestCase`

- `python manage.py test [app]` - run python test suite; optional run test for only given [app]

- [Deploying a Django App to Heroku](https://www.codementor.io/@jamesezechukwu/how-to-deploy-django-app-on-heroku-dtsee04d4)

- Pages app deployed [here](https://page-cm.herokuapp.com/)

### HowTo Deploy Django App to Heroku

1. `Procfile` -> `web: gunicorn project_name.wsgi --log-file -`
2. `runtime.txt` -> `python-major.minor.patch`
3. `pip install gunicorn dj-database-url whitenoise psycopg2`
4. `pip freeze > requirements.txt`
5. Edit bottom of `settings.py` to handle static assets:

```python
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/
PROJECT_ROOT   =   os.path.join(os.path.abspath(__file__))
STATIC_ROOT  =   os.path.join(PROJECT_ROOT, 'staticfiles')
STATIC_URL = '/static/'

# Extra lookup directories for collectstatic to find static files
STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT, 'static'),
)

#  Add configuration for static files storage using whitenoise
STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'
```

6. Add `whitenoise.middleware.WhiteNoiseMiddleware` to top of `MIDDLEWARE` list in `settings.py`
7. Update database configuration at bottom of `settings.py`

```python
import dj_database_url

prod_db = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(prod_db)
```

8. `heroku create [heroku-app-name]`; Remember to `heroku login`
9. Add `[heroku-app-name.herokuapp.com]` to `ALLOWED_HOSTS` list in `settings.py`
10. Add heroku remote repo to git: `heroku git:remote -a heroku-app-name`
11. Push code to heroku: `git add -A; git commit -m 'Initial commit'; git push heroku master`
12. If error: `heroku config:set DISABLE_COLLECTSTATIC=1` tells Heroku to ignore static files
13. Migrate database on heroku: `heroku run python manage.py migrate`
14. `heroku ps:scale web=1`

## Chapter 04: Message Board App

- Site will allow users to create post (via admin panel) and read short messages

- Introduces Django's built-in admin interface - visual way to make changes to our data

- The project will use SQLite for development and PostgreSQL in production on Heroku.

- Whenever one creates or modifies an existing model:

  1. `python manage.py makemigrations [model]`
  2. `python manage.py migrate`

- To use Django admin:

  1. Create a superuser: `python manage.py createsuperuser`
  2. Navigate to `http://localhost:8000/admin/`

### Generic Class-Views

- `TemplateView` -> Allows a view to link a url to a template via `template_name`

- `ListView` -> returns all instances of a db model; view needs `model`, `template_name`, `context_object_name`

- **Best Practice** Add `__str__` methods to all models to improve readability

- All test methods should begin with `test_`

- `Django for Professionals` covers how to deploy on Heroku using PostgreSQL instead of SQLite

- Message Board App deployed [here](https://mb-app-cm.herokuapp.com/)
  - ??? Why aren't local development db records (the posts I created locally) transferred over?

## Chapter 05: Blog App

- `ForeignKey` field type establishes a _many-to-one_ relationship to a remote model
  - required arguments: `string path to model`, `on_delete` action

```python
class Post(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(
        'auth.User',
        on_delete=models.CASCADE
    )
    body = models.TextField()

    def __str__(self):
        return self.title
```

- **Best Practice** always `name` your URLConfs. Helps to keep view code tidy.

- **VS CODE settings** add `django-html` to Auto Close Tag and Beautify extensions to add support for Django Templates

```json
"auto-close-tag.activationOnLanguage": [
  "xml",
  "php",
  "blade",
  "ejs",
  "jinja",
  "javascript",
  "javascriptreact",
  "typescript",
  "typescriptreact",
  "plaintext",
  "markdown",
  "vue",
  "liquid",
  "erb",
  "lang-cfml",
  "cfml",
  "HTML (Eex)",
  "django-html"
],
"beautify.language": {
  "js": {
    "type": [
      "javascript",
      "json",
      "jsonc"
    ],
    "filename": [
      ".jshintrc",
      ".jsbeautifyrc"
    ]
  },
  "css": [
    "css",
    "less",
    "scss"
  ],
  "html": [
    "htm",
    "html",
    "django-html"
  ]
}
```

- Test methods encountered: `assertEqual`, `assertContains`, `assertTemplateUsed`

- From `django.contrib.auth`, helper method `get_user_model` ... returns built-in Django `auth.User` model
  - ??? `create_user` vs `create`

## Chapter 06: Forms

- **Best Practice** `{% csrf_token %}` always included csrf_token when using Django forms
  - Results in _hidden_ input element

```html
<input
  type="hidden"
  name="csrfmiddlewaretoken"
  value="mRblhzLP1CDxfvHXzQyq70UZyav91YIsDtGW82EHKJE4dDVYNNfKi4DyQuQq2tNE"
/>
```

- **Best Practice** Sending data, `method="post"` in form tag; Receiving data, `method="get"` in form tag

- **Best Practice** Always add `get_absolute_url` method to db models

```python
def get_absolute_url(self):
    return reverse("post_detail", args=[str(self.id)])
```

- `args` must be an iterable, hence the `[ ]`

- **Best Practice** Add a `test_get_absolute_url` method

- **Best Practice** Add test for all `CRUD` methods used by a model

## Chapter 07: User Accounts

- Focus on `user authentication`

- `auth.User` contains:

  1. `username`
  2. `password`
  3. `email`
  4. `first_name`
  5. `last_name`

- Project will use `User` model to implement _log in_, _log out_, and _sign up_

- Create `accounts` app to manage `auth.User` registration

  - Used `django.contrib.auth.form.UserCreationForm` and `django.views.generic.CreateView`

- Under `settings.py`, added `LOGIN_REDIRECT_URL` and `LOGOUT_REDIRECT_URL` to facilitate login/logout flow

- Blog app deployed [here](https://blog-app-cnml.herokuapp.com/)

## Chapter 08: Custom User Model ([GitHub Newspaper App](https://gitlab.com/cesar29/dfb-news))

- **Important** DO NOT LAUNCH MIGRATIONS until `CustomUser` flow complete

- **Best Practice** always use a custom user model for new projects

- Django was originally built for editors and journalist at the `Lawrence Journal-World`

- Custom user app sometimes called `accounts` instead of `users`

- `null=True` ... **database-related** setting ... allows NULL in field record

- `blank=True` ... **validation-related** seting ... allows form field to be empty

- **Best Practice** for string-based fields; only `blank=True`; Empty string indicates absence of value not NULL.

- _To add our custom `age` field we simply tack it on at the end and it will display automatically on our future sign up page. Pretty slick, no?_

- _The concept of fields on a form can be confusing at first so let's take a moment to explore it further._

- _We will extend the existing `UserAdmin` class to use our new `CustomUser` model_

- **Breakdown Django Admin panel into React components**

- _In the next chapter we will configure and customize sign up, log in, and log out pages_

- Prefer `CustomUser` to inherit from `AbstractUser` instead of `AbstractBaseUser` -> less hassle

- `forms.py` -> Extends `UserCreationForm` -> `CustomUserCreationForm` and `UserChangeForm` -> `CustomUserChangeForm` since `User` extended to `CustomUser`

  - Uses `class Meta` to extend fields; this subclass inherits from appropriate form
  - ??? Why:

  ```python
  class CustomUserChangeForm(UserChangeForm):

    # why not? ... fields = UserChangeForm.Meta.fields + ('age',); explicitly add custom field
    class Meta:
      model = CustomUser
      fields = UserChangeForm.Meta.fields
  ```

- `UserCreationForm` default values are `username`, `email`, and `password`

- In `admin.py`, we need to extend `django.contrib.auth.admin.UserAdmin` -> `CustomUserAdmin`

  - Need to set: `model`, `add_form`, `form` - custom creation and change form, respectively

- As before, register models: `admin.site.register(CustomUser, CustomUserAdmin)`

- `list_display` alters fields displayed in Django admin panel; property of CustomUserAdmin; list of fields

## Chapter 09: User Authentication

- Add `templates` dir to root of project to avoid app template nesting pattern

  - Requires edit to `settings.py`, `TEMPLATES`: `'DIRS': [os.path.join(BASE_DIR, 'templates')],`

- To redirect after login/logout, set `LOGIN_REDIRECT_URL` and `LOGOUT_REDIRECT_URL` in `settings.py`

- **NOTE** Django expects `login.html` template to be within `registration` dir (e.g. `templates/registration/login.html`)

- When using a custom user, `django.contrib.auth.urls` handles login/logout urls, views, and templates

  - e.g. `path('users/', include('django.contrib.auth.urls'))` .. similar to how `path('admin/', admin.site.urls)` handles admin urls, views, and templates

- `SignUpView` inherits from `django.views.generic.CreateView`

  - Requires `form_class`, `success_url`, and `template_name` to be declared

  ```python
  class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'
  ```

## Chapter 10: Bootstrap & HomePageView/SignUpView Test Suite

- Need to test `home` and `signup` pages; `login` and `logout` are handled by Django core features

- Testing Pattern. For each static page, tutorial test 3 things:

  1. Test `self.client.get` to hardcoded url returns correct status code
  2. Test `self.client.get` to `named url` return correct status code
  3. Test `self.client.get` uses correct named template (i.e. `self.assertTemplateUsed`)

- SignUp Test. Reference user model with `django.contrib.auth.get_user_model` method
- Uses auth.User objects manager `create_user` method to create and save a user to test db

  - e.g. `get_user_model().objects.create_user(self.username, self.email)` ... _weird that password isn't required_
  - Test further check object count equal 1, and username, email attributes set correctly

- Since we are using `React` instead of templates to generate FE pages, not much interesting in this chapter
  - Used `django-crispy-forms` to change default form styling
  - Requires adding `crispy-forms` to `INSTALLED_APPS` in `settings.py`
    - Also need to set `CRISPY_TEMPLATE_PACK = 'bootstrap4'` in `settings.py`
    - In `signup` template, `{% load crispy_forms_tags %}` and replace `{{ form.as_p }}` with `{{ form|crispy }}`

## Chapter 11: Password Change and Reset

- Enable password change and password reset views

- Template heavy chapter; Mainly spent time customizing `password_change` and `password_reset` templates (flow below):

  - `password_change_form.html`
  - `password_change_done.html`
  - `password_reset_form.html`
  - `password_reset_done.html`
  - `password_reset_confirm.html`
  - `password_reset_complete.html`

- For development, in `settings.py`, set `EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'`
  - outputs password reset email to console.

## Chapter 12: Email

- _At this point you may be feeling a little overwhelmed by all the user authentication configuration we've done up to this point. That's normal._

- _As you become more and more experienced in web development, the wisdom of Django's approach will ring true._

- In chapter 11, password_reset emails were being sent to the console, let's fix that.

- Will implement SendGrid to enable SMTP sending of password reset emails to users

- **Pretty sure I should be hiding my keys in environmental variables.**

- In `settings.py`, change/add:

  - `EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

  ```python
  EMAIL_HOST = 'smtp.sendgrid.net'
  EMAIL_HOST_USER = 'apikey'
  EMAIL_HOST_PASSWORD = '[SEND_GRID_API_KEY]'
  EMAIL_PORT = 587
  EMAIL_USE_TLS = True
  ```

- `registration/password_reset_email.html` change if you wish to customize template

  - ??? How to change `site_name` referenced in above template? `reset_link` block; `token`

- `registration/password_reset_subject.txt` to change email subject line

- ??? I would like to customize the variables available to the password reset email template

## Chapter 13: Newspaper App (not just auth and password 🙃)

- PSD:
  - A journalist will be able to post articles.
  - Only the author (a journalist identified by `pk` (Many-to-One FK on Article model link to `pk` on Journalist model)) of an article will be able to edit or delete an article
  - A user will be able to post comments to an article (Many-to-One FK on Comments link to `pk` on Article model)

## Chapter 14: Permissions and Authorization

- _Authorization_ restricts access; _authentication_ enables a user sign up and log in flow.

- We'll limit web page access to logged-in users.

- _The more you use and customize built-in views, the more comfortable you will become making customizations like this._

- Add `form_valid` method to `ArticleCreateView`

```python
def form_valid(self, form):
    form.instance.author = self.request.user
    return super().form_valid(form)
```

- **Mixins** a special kind of multiple inheritance that Django uses to avoid duplicate code and skill allows customization.

- `LoginRequiredMixin` from `django.contrib.auth.mixins` can be used to redirect not logged-in users to the login page on protected views

- `UserPassesTestMixin` from `django.contrib.auth.mixins` can be used authorize users via `test_func`

- ??? Improvement, only show `Edit` and `Delete` to properly authorized users

## Chapter 15: Comments

- Additional PSD:

  - Users will be able to add comments to any user article.

- **Best Practice** Keep each migration as small and contained as possible by designating which app migrations should run on

- _Understanding queries takes some time so don't be concerned if the idea of reverse relationships is confusing_

- **Further Work** Add Article Creation Form on `articles/` as well as ability to add `comments` directly on `article_detail.html` template

  - Add `age dropdown` to SignUp form; restrict sign up to >= 13 years old
  - Switch to `postgreSQL` for deployment
  - Use environmental variables (`activate`) to hide secret keys
  - Add `discounts` to users >= 65 years old (Django for Professionals - `payments` app)
  - Turn `Newspaper` app to a `Todo List` app and allow for user registration

- _Most of web development follows the same patterns and by using a web framework like Django 99% of what we want in terms of functionality is either already included or only a small customization of an existing feature away_

## Conclusion

- Finished Django for Beginners (TODO: go back and add more notes)

- _Web development is a very deep field and there's always something new to learn. When you're starting out I believe the best approach is to build as many small projects as possible and incrementally add complexity and research new things._

- `Django for Professional` tackles _production-ready_ challenges:

  - `Deploying with Docker`
  - `Provisioning PostgreSQL` database
  - `Handling payments`
  - `Environment variables`
  - `Advanced user registration`
  - `Security`
  - `Performance`

- `Django for APIs`

  - _Creating a full-stack website is quite the challenge for a single developer._
  - _However if you talk to professional Django developers who work at either small startups or large corporations, chances are the majority of their time is spent solely on the back-end creating Django-based web APIs. 🤷‍♀️_

- Open-Source Resources (Curated by the author of this book trilogy, William Vincent):

  - [Awesome-Django](https://github.com/wsvincent/awesome-django)
  - [DjangoX](https://github.com/wsvincent/djangox)
    - A framework for launching new Django projects quickly. Comes with a custom user model, email/password authentication, options for social authentication via Google/Facebook/Twitter/etc, and static assets.
  - [DRFX](https://github.com/wsvincent/drfx)
    - A framework for launching new Django Rest Framework projects quickly.

- Don't fear using [Django documentation](https://www.djangoproject.com/) and [source code](https://github.com/django/django) as a resource as Django-fu further develops

- [Classy Class-Based Views](https://ccbv.co.uk/)

  - Detailed descriptions, with full methods and attributes, for each of Django's class-based generic views.

- When time allows:

  - [DjangoCon conference talks](https://www.youtube.com/playlist?list=PL2NFhrDSOxgXmA215-fo02djziShwLa6T)
  - [Two Scoops of Django](https://www.amazon.com/gp/product/0692915729/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=wsvincent-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=0692915729&linkId=42ac7eb52adc397a78080f12f7f839b1)

    - Best Practices Bible for Django

  - Python Resources:

    - Beginners:

      - Eric Matthes's [Python Crash Course](https://www.amazon.com/gp/product/1593276036/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=wsvincent-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=1593276036&linkId=1f0cfaa42d12dcf4de967c31ccaad831)
      - Al Sweigart's [Automate the Boring Stuff](https://www.amazon.com/gp/product/1593275994/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=wsvincent-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=1593275994&linkId=10e3efd8d2bc01606996d30eab8bbd8e)

    - Intermediate to Advance:
      - [Fluent Python](https://www.amazon.com/gp/product/1491946008/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=wsvincent-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=1491946008&linkId=9d73ed7c4523ffd8f1c2380672de0c3b), [Effective Python](https://www.amazon.com/gp/product/0134034287/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=wsvincent-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=0134034287&linkId=6c823c0137898e6e7fabfc245f387eaa), and [Python Tricks](https://www.amazon.com/gp/product/1775093301/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=wsvincent-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=1775093301&linkId=909a50c337bebad60a277b334c4bd1a2)
