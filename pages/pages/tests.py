from django.test import SimpleTestCase

# Create your tests here.


class SimpleTests(SimpleTestCase):
    def test_home_page_status_code(self):
        """
        Assert that '/' homepage returns status_code 200
        """
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_about_page_status_code(self):
        """
        Assert that '/about/' page returns status_code 200
        """
        response = self.client.get('/about/')
        self.assertEqual(response.status_code, 200)
